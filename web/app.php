<?php

umask(0002);

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';

// Use APC for autoloading to improve performance
// Change 'sf2' by the prefix you want in order to prevent key conflict with another application
/*
$loader = new ApcClassLoader('sf2', $loader);
$loader->register(true);
*/

require_once __DIR__.'/../app/AppKernel.php';
//require_once __DIR__.'/../app/AppCache.php';


if (!isset($_SERVER['SYMFONY_ENV'])) {
    trigger_error('The vhost is missing the SYMFONY_ENV variable.', E_USER_ERROR);
}

$debug = in_array($_SERVER['SYMFONY_ENV'], array('dev'));
$kernel = new AppKernel($_SERVER['SYMFONY_ENV'], $debug);

if ($debug) {
    Debug::enable();
}

$kernel->loadClassCache();

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
