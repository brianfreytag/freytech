$(document).ready(function() {
    $('[rel=tooltip]').tooltip();

     $('.open-left').on('click', function(e) {
         e.preventDefault();
        $('.left-nav-inside').toggle("slide", {direction: 'right'});
    });

    $('.open-right').on('click', function(e) {
        e.preventDefault();
        $('.right-nav-inside').toggle("slide");
    });

    $('.open-left-sidebar').on('click', function(e) {
        e.preventDefault();
        $('.sidebar-left .navigation-block').toggle("slide");
    });

});
