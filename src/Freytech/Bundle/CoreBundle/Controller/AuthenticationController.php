<?php

namespace Freytech\Bundle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

class AuthenticationController extends Controller
{
    /**
     * @param Request $request
     * @param $source
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function signInAction(Request $request, $source)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER') && $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            return $this->redirect($this->generateUrl('freytech_core.homepage'));
        }

        $session = $request->getSession();

        $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        $session->remove(SecurityContext::AUTHENTICATION_ERROR);

        $lastUsername = $session->get(SecurityContext::LAST_USERNAME);

        $content = array(
            'source'        => $source,
            'last_username' => $lastUsername,
            'has_error'     => $error ? true : false,
        );

        if ($hash = $session->get('verification')) {
            $content['verification_hash'] = $hash;
        }

        return $this->render('FreytechCoreBundle:Authentication:signIn.html.twig', $content);
    }

    public function authenticateAction()
    {
        return $this->redirect($this->generateUrl('freytech_common.sign_in'));
    }

    public function signOutAction()
    {
        // Handled by securitas
    }
}
