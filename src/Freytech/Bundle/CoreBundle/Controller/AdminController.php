<?php

namespace Freytech\Bundle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_VERIFIED')")
     */
    public function homeAction(Request $request)
    {
        return $this->render('FreytechCoreBundle:Admin:home.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_VERIFIED')")
     */
    public function accountAction(Request $request)
    {
        $user = $this->getUser();

        $formHandler = $this->get('freytech_admin.form.handler.update_user');

        $formHandler->process($request, $user);

        return $this->render(
            'FreytechCoreBundle:Admin:account.html.twig',
            array(
                'user' => $user,
                'form' => $formHandler->getForm()->createView(),
            )
        );
    }
}
