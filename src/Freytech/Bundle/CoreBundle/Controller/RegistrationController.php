<?php

namespace Freytech\Bundle\CoreBundle\Controller;

use Freytech\Bundle\CommonBundle\Entity\User;
use Freytech\Bundle\CommonBundle\Entity\UserRole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * Handles all functionality of registration and account verification
 *
 * @author Brian Freytag <brian@quizzle.com>
 * @package Freytech\Bundle\CoreBundle\Controller
 */
class RegistrationController extends Controller
{
    /**
     * Handles Registration
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $formHandler = $this->get('freytech_common.form.handler.registration');
        if ($formHandler->process($request)) {
            return $this->redirect($this->generateUrl('freytech_common.registration.thank_you'));
        }

        return $this->render(
            'FreytechCoreBundle:Registration:register.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
            )
        );
    }

    /**
     * Contains the instructions for verifying their email
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function thankYouAction(Request $request)
    {
        if (!$request->getSession()->getFlashBag()->has('registration-email')) {
            throw $this->createNotFoundException('Unable to access email confirmation page without completing registration.');
        }

        $request->getSession()->getFlashBag()->get('registration-email');

        return $this->render(
            'FreytechCoreBundle:Registration:thankYou.html.twig'
        );
    }

    /**
     * Handles email verification
     *
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     */
    public function accountVerifyAction(Request $request, $hash)
    {
        $userRepository = $this->get('freytech_common.repository.user');

        if (!$user = $userRepository->findByAuthenticationHash($hash)) {
            throw $this->createNotFoundException('User couldn\'t be found with authentication hash');
        }

        $failed = false;

        if (!$this->get('freytech_common.email_verification')->verify($user)) {
            $failed = true;
        }

        return $this->render('FreytechCoreBundle:Registration:accountVerify.html.twig', array(
            'firstName' => $user->getFirstName(),
            'failed' => $failed,
        ));

    }
}
