<?php

namespace Freytech\Bundle\CoreBundle\Controller;

use Freytech\Bundle\CommonBundle\Entity\Core\ContactSubmission;
use Freytech\Bundle\CommonBundle\Form\Type\ContactSubmissionType;
use Freytech\Bundle\CommonBundle\Form\Handler\ContactSubmissionFormHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FreytechCoreBundle:Default:index.html.twig');
    }

    public function aboutAction()
    {
        return $this->render('FreytechCoreBundle:Default:about.html.twig');
    }

    public function contactAction(Request $request)
    {
        /** @var ContactSubmissionFormHandler $formHandler */
        $formHandler = $this->get('freytech_common.form.handler.contact_submission');
        if ($formHandler->process($request)) {
            return $this->redirect($this->generateUrl('freytech_core.contact'));
        }

        return $this->render('FreytechCoreBundle:Default:contact.html.twig', array('form' => $formHandler->getForm()->createView()));
    }

    public function resumeAction()
    {
        return $this->render('FreytechCoreBundle:Default:resume.html.twig');
    }
}
