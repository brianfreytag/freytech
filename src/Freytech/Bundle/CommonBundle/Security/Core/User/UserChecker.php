<?php


namespace Freytech\Bundle\CommonBundle\Security\Core\User;

use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Freytech\Bundle\CommonBundle\Entity\Manager\UserManager;

class UserChecker implements UserCheckerInterface
{
    /** @var \Freytech\Bundle\CommonBundle\Entity\Manager\UserManager */
    protected $userManager;

    /**
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    public function checkPreAuth(UserInterface $user)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function checkPostAuth(UserInterface $user)
    {
        if ($this->userManager->isLocked($user)) {
            $lockedException = new LockedException('User account is locked.');
            $lockedException->setUser($user);

            throw $lockedException;
        }

        if (!$user->isActive()) {
            $disabledException = new DisabledException('User account is disabled.');
            $disabledException->setUser($user);

            throw $disabledException;
        }
    }
}