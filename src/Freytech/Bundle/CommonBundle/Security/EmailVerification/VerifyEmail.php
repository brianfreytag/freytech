<?php

namespace Freytech\Bundle\CommonBundle\Security\EmailVerification;

use Doctrine\ORM\EntityManagerInterface;
use Freytech\Bundle\CommonBundle\Entity\Repository\UserRepository;
use Freytech\Bundle\CommonBundle\Entity\User;

class VerifyEmail
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;
    /** @var \Freytech\Bundle\CommonBundle\Entity\Repository\UserRepository */
    private $userRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     */
    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function verify(User $user)
    {
        if (!$user->isActive()) {
            return false;
        }

        $user->setIsVerified(true);
        $user->setVerificationDate(new \DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return true;
    }
}
