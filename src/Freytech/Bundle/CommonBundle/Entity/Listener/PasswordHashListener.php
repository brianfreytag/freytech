<?php

namespace Freytech\Bundle\CommonBundle\Entity\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Freytech\Bundle\CommonBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class PasswordHashListener
{
    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof User && 0 !== strlen($entity->getPlainPassword())) {
            $encoder = $this->encoderFactory->getEncoder($entity);
            $entity->setPassword($encoder->encodePassword($entity->getPlainPassword(), $entity->getSalt()));
        }
    }
}