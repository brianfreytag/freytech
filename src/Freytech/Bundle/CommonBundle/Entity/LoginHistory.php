<?php

namespace Freytech\Bundle\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation\Timestampable;

/**
 * Freytech\Bundle\CommonBundle\Entity\LoginHistory
 *
 * @ORM\Table(name="loginHistory")
 * @ORM\Entity(repositoryClass="Freytech\Bundle\CommonBundle\Entity\Repository\LoginHistoryRepository")
 */
class LoginHistory
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="loginHistoryId", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Freytech\Bundle\CommonBundle\Entity\User $user
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="userId")
     */
    private $user;

    /**
     * @var bool $success
     *
     * @ORM\Column(name="isSuccess", type="boolean")
     */
    private $success;

    /**
     * @var int $previousFailedAttempts
     *
     * @ORM\Column(name="previousFailedAttempts", type="integer")
     */
    private $previousFailedAttempts;

    /**
     * @var string $ipAddress
     *
     * @ORM\Column(name="ipAddress", type="string", length=30)
     */
    private $ipAddress;

    /**
     * @var \DateTime $lastUpdateDate
     *
     * @Timestampable(on="update")
     * @ORM\Column(name="lastUpdateDate", type="datetime")
     */
    private $lastUpdateDate;

    /**
     * @var \DateTime $createDate
     *
     * @Timestampable(on="create")
     * @ORM\Column(name="createDate", type="datetime")
     */
    private $createDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \Freytech\Bundle\CommonBundle\Entity\User $user
     *
     * @return LoginHistory
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \Freytech\Bundle\CommonBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param string $previousFailedAttempts
     */
    public function setPreviousFailedAttempts($previousFailedAttempts)
    {
        $this->previousFailedAttempts = $previousFailedAttempts;
    }

    /**
     * @return string
     */
    public function getPreviousFailedAttempts()
    {
        return $this->previousFailedAttempts;
    }

    /**
     * @param string $ipAddress
     *
     * @return LoginHistory
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdateDate()
    {
        return $this->lastUpdateDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }
}
