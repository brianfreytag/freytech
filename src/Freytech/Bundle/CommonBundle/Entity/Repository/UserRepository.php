<?php

namespace Freytech\Bundle\CommonBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
    public function findByUsername($username)
    {
        return $this->findOneBy(array('username' => $username));
    }

    public function findByEmail($email)
    {
        return $this->findOneBy(array('email' => $email));
    }

    public function findByAuthenticationHash($hash)
    {
        $queryBuilder = $this->createQueryBuilder('u');

        $query = $queryBuilder->select('u')
            ->where($queryBuilder->expr()->eq('u.authenticationHash', ':authenticationHash'))
            ->andWhere($queryBuilder->expr()->eq('u.isVerified', ':verified'))
            ->setParameter('authenticationHash', $hash)
            ->setParameter('verified', false)
            ->getQuery();

        return $query->getOneOrNullResult();
    }
}
