<?php

namespace Freytech\Bundle\CommonBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Freytech\Bundle\CommonBundle\Entity\LoginHistory;
use Freytech\Bundle\CommonBundle\Entity\User;

/**
 * @author Jeremy Livingston <jeremy@quizzle.com>
 */
class LoginHistoryRepository extends EntityRepository
{
    /**
     * @param \Freytech\Bundle\CommonBundle\Entity\User $user
     *
     * @return \Freytech\Bundle\CommonBundle\Entity\LoginHistory|null
     */
    public function findLatest(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('l');
        $query = $queryBuilder->select('l')
            ->where($queryBuilder->expr()->eq('l.user', ':user'))
            ->andWhere($queryBuilder->expr()->eq('l.success', $queryBuilder->expr()->literal(true)))
            ->addOrderBy('l.createDate', 'desc')
            ->setMaxResults(1)
            ->setParameter('user', $user)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param \Freytech\Bundle\CommonBundle\Entity\User $user
     *
     * @return \Freytech\Bundle\CommonBundle\Entity\LoginHistory|null
     */
    public function findPrevious(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('l');
        $query = $queryBuilder->select('l')
            ->where($queryBuilder->expr()->eq('l.user', ':user'))
            ->andWhere($queryBuilder->expr()->eq('l.success', $queryBuilder->expr()->literal(true)))
            ->addOrderBy('l.createDate', 'desc')
            ->setFirstResult(1)
            ->setMaxResults(1)
            ->setParameter('user', $user)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param \Freytech\Bundle\CommonBundle\Entity\User $user
     * @param int $maxResults
     *
     * @return array
     */
    public function getLoginHistory(User $user, $maxResults = 20)
    {
        $queryBuilder = $this->createQueryBuilder('l');
        $query = $queryBuilder->select('l')
            ->where($queryBuilder->expr()->eq('l.user', ':user'))
            ->addOrderBy('l.createDate', 'desc')
            ->setParameter('user', $user)
            ->setMaxResults($maxResults)
            ->getQuery();

        return $query->getArrayResult();
    }

    /**
     * Get the number of logins for a user after the provided date
     *
     * @param \Freytech\Bundle\CommonBundle\Entity\User $user
     * @param \DateTime $date
     *
     * @return int
     */
    public function findCountAfterDate(User $user, \DateTime $date)
    {
        $queryBuilder = $this->createQueryBuilder('l');
        $query = $queryBuilder->select($queryBuilder->expr()->count('l.id'))
            ->where($queryBuilder->expr()->eq('l.user', ':user'))
            ->andWhere($queryBuilder->expr()->eq('l.success', $queryBuilder->expr()->literal(true)))
            ->andWhere($queryBuilder->expr()->gt('l.createDate', ':date'))
            ->setParameter('user', $user)
            ->setParameter('date', $date)
            ->getQuery();

        return $query->getSingleScalarResult();
    }
}
