<?php


namespace Freytech\Bundle\CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Quizzle\Bundle\CommonBundle\Entity\User
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user", indexes={@ORM\Index(name="username_email_active_failed_verified_admin_collaborator_hash", columns={"username", "email", "isActive", "failedAttempts", "isVerified", "isAdmin", "isCollaborator", "authenticationHash"})})
 * @ORM\Entity(repositoryClass="Freytech\Bundle\CommonBundle\Entity\Repository\UserRepository")
 * @Assert\Callback(methods={"isPasswordNotEqualToEmail", "isPasswordNotEqualToUsername"})
 * @UniqueEntity("username", message="This username already exists.")
 * @UniqueEntity("email", message="This email already exists.")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="userId", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "You must enter a username.", groups = {"registration","update_user"})
     * @ORM\Column(name="username", type="string", length=75)
     *
     */
    private $username;

    /**
     * @var string $email
     *
     * @Assert\NotBlank(message = "You must enter a valid email address.", groups = {"registration", "update_user"})
     * @Assert\Email(message = "You must enter a valid email address.", groups = {"registration", "update_user"})
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * The encrypted password
     *
     * @ORM\Column(name="password", type="string", length=40)
     */
    private $password;

    /**
     * The plain password for registration. Shouldn't be persisted
     *
     * @Assert\NotBlank(message = "Pick a password that's super easy for you to remember, but impossible for someone else to figure out.", groups = {"registration", "password_reset"})
     * @Assert\Regex(pattern = "/^[a-zA-Z0-9~!@#%:;<>\x22\\\,\/\.\[\]\$\(\)\|\-\*\?\{\}&_\+=\^]{6,20}$/", groups = {"registration", "password_reset"}, message = "Your password must be 6-20 characters and cannot contain spaces or anything else too wacky.")
     */
    private $plainPassword;

    /**
     * @var string $firstName
     *
     * @Assert\NotBlank(message = "You must enter your first name.")
     * @Assert\Length(max = 30)
     * @Assert\Regex(pattern = "/^[a-zA-Z][a-zA-Z\.\,\-'\ ]*$/", message = "Please enter a valid first name")
     * @ORM\Column(name="firstName", type="string", length=30)
     */
    private $firstName;

    /**
     * @var string $lastName
     *
     * @Assert\NotBlank(message = "You must enter your last name")
     * @Assert\Length(max = 30)
     * @Assert\Regex(pattern = "/^[a-zA-Z][a-zA-Z\.\,\-'\ ]*$/", message = "You must enter a valid last name")
     * @ORM\Column(name="lastName", type="string", length=30)
     */
    private $lastName;

    /**
     * @var string $displayName
     *
     * @Assert\Length(max = 80)
     *
     * @ORM\Column(name="displayName", type="string", length=80, nullable=true)
     */
    private $displayName;

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="isVerified", type="boolean")
     */
    private $isVerified = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isAdmin", type="boolean")
     */
    private $isAdmin = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isCollaborator", type="boolean")
     */
    private $isCollaborator = false;

    /**
     * @var string
     *
     * @ORM\Column(name="authenticationHash", type="string", length=40)
     */
    private $authenticationHash;

    /**
     * @ORM\Column(name="failedAttempts", type="integer")
     */
    private $failedAttempts = 0;

    /**
     * @var \DateTime $lastFailedDate
     *
     * @ORM\Column(name="lastFailedDate", type="datetime", nullable=true)
     */
    private $lastFailedDate;

    /**
     * @var \DateTime $verificationDate
     *
     * @ORM\Column(name="verificationDate", type="datetime", nullable=true)
     */
    private $verificationDate;

    /**
     * @var \DateTime $createDate
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createDate", type="datetime")
     */
    private $createDate;

    /**
     * @var \DateTime $lastUpdateDate
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="lastUpdateDate", type="datetime")
     */
    private $lastUpdateDate;

    /**
     * Make sure the user's password isn't equal to their email address
     *
     * @param ExecutionContextInterface $context
     */
    public function isPasswordNotEqualToEmail(ExecutionContextInterface $context)
    {
        if (!empty($this->password) && $this->password === $this->email) {
            $context->addViolation('password', 'Your password cannot be the same as your email address.', array(), null);
        }
    }

    /**
     * Make sure the user's password isn't equal to their username
     *
     * @param ExecutionContextInterface $context
     */
    public function isPasswordNotEqualToUsername(ExecutionContextInterface $context)
    {
        if (!empty($this->password) && $this->password === $this->username) {
            $context->addViolation('password', 'Your password cannot be the same as your username.', array(), null);
        }
    }

    /**
     * Set the AuthHash based on the current object's data
     *
     * @ORM\PrePersist
     */
    public function processAuthenticationHash()
    {
        $this->setAuthenticationHash(sha1(uniqid($this->getEmail() . mt_rand(), true)));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        if (!$this->displayName) {
            $this->displayName = $this->username;
        }

        return $this->displayName;
    }

    public function getDisplayNames()
    {
        return array(
            $this->username                                  => $this->username,
            $this->firstName . ' ' . $this->lastName         => $this->firstName . ' ' . $this->lastName,
            $this->firstName . substr($this->lastName, 0, 1) => $this->firstName . substr($this->lastName, 0, 1),
            $this->firstName                                 => $this->firstName,
            $this->lastName . ' ' . $this->firstName         => $this->lastName . ' ' . $this->firstName,
            substr($this->firstName, 0, 1) . $this->lastName => substr($this->firstName, 0, 1) . $this->lastName,
        );
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isAdmin
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
    }

    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param boolean $isCollaborator
     */
    public function setIsCollaborator($isCollaborator)
    {
        $this->isCollaborator = $isCollaborator;
    }

    /**
     * @return boolean
     */
    public function isCollaborator()
    {
        return $this->isCollaborator;
    }



    /**
     * @param boolean $isVerified
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return boolean
     */
    public function isVerified()
    {
        return $this->isVerified;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = array();

        if ($this->isActive()) {
            $roles[] = 'ROLE_USER';
        }

        if ($this->isVerified()) {
            $roles[] = 'ROLE_VERIFIED';
        }

        if ($this->isAdmin()) {
            $roles[] = 'ROLE_ADMIN';
        }

        if($this->isCollaborator()) {
            $roles[] = 'ROLE_COLLABORATOR';
        }

        return $roles;
    }

    /**
     * @param string $authenticationHash
     */
    public function setAuthenticationHash($authenticationHash)
    {
        $this->authenticationHash = $authenticationHash;
    }

    /**
     * @return string
     */
    public function getAuthenticationHash()
    {
        return $this->authenticationHash;
    }

    /**
     * @param mixed $failedAttempts
     */
    public function setFailedAttempts($failedAttempts)
    {
        $this->failedAttempts = $failedAttempts;
    }

    /**
     * @return mixed
     */
    public function getFailedAttempts()
    {
        return $this->failedAttempts;
    }

    /**
     * @return User
     */
    public function addFailedAttempt()
    {
        $this->failedAttempts += 1;
        return $this;
    }

    /**
     * @param \DateTime $lastFailedDate
     */
    public function setLastFailedDate($lastFailedDate)
    {
        $this->lastFailedDate = $lastFailedDate;
    }

    /**
     * @return \DateTime
     */
    public function getLastFailedDate()
    {
        return $this->lastFailedDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate($verificationDate)
    {
        $this->verificationDate = $verificationDate;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate()
    {
        return $this->verificationDate;
    }


    /**
     * @param \DateTime $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param \DateTime $lastUpdateDate
     */
    public function setLastUpdateDate($lastUpdateDate)
    {
        $this->lastUpdateDate = $lastUpdateDate;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdateDate()
    {
        return $this->lastUpdateDate;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return '';
    }

    public function eraseCredentials()
    {
    }


//    public function isEqualTo(UserInterface $user)
//    {
//        return $this->getRoles() === $user->getRoles() && $this->getUsername() === $user->getUsername();
//    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->plainPassword,
            $this->firstName,
            $this->lastName,
            $this->displayName,
            $this->isActive,
            $this->authenticationHash,
            $this->failedAttempts,
            $this->lastFailedDate,
            $this->createDate,
            $this->lastUpdateDate,
        ));
    }

    /**
     * @param $data
     */
    public function unserialize($data)
    {
        list(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->plainPassword,
            $this->firstName,
            $this->lastName,
            $this->displayName,
            $this->isActive,
            $this->authenticationHash,
            $this->failedAttempts,
            $this->lastFailedDate,
            $this->createDate,
            $this->lastUpdateDate,
        ) = unserialize($data);
    }

}
