<?php

namespace Freytech\Bundle\CommonBundle\Entity\Manager;

use Freytech\Bundle\CommonBundle\Entity\User;

class UserManager
{
    /** @var int */
    protected $maxFailedAttempts;

    /** @var int */
    protected $lockMinutes;

    /**
     * @param $maxFailedAttempts
     * @param $lockMinutes
     */
    public function __construct($maxFailedAttempts, $lockMinutes)
    {
        $this->maxFailedAttempts = $maxFailedAttempts;
        $this->lockMinutes = $lockMinutes;
    }

    /**
     * Checks to see if the user is locked out due to failed attempts and within the lockout time frame
     *
     * @param User $user
     * @return bool
     */
    public function isLocked(User $user)
    {
        $now = new \DateTime();
        $now->sub(new \DateInterval('PT' . $this->lockMinutes . 'M'));

        if ($user->getFailedAttempts() >= $this->maxFailedAttempts && $user->getLastFailedDate() && $now <= $user->getLastFailedDate()) {
            return true;
        }

        return false;
    }
}