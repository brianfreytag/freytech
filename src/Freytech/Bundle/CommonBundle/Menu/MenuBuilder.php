<?php

namespace Freytech\Bundle\CommonBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * @author Jeremy Livingston <jeremy@quizzle.com>
 */
class MenuBuilder
{
    /** @var \Knp\Menu\FactoryInterface */
    private $factory;
    /** @var  SecurityContextInterface */
    private $context;

    /**
     * @param FactoryInterface $factory
     * @param SecurityContextInterface $context
     */
    public function __construct(FactoryInterface $factory, SecurityContextInterface $context)
    {
        $this->factory = $factory;
        $this->context = $context;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function leftMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setCurrentUri($request->getRequestUri());

        $menu->setChildrenAttributes(array('class' => 'nav nav-stacked'));

        $aboutSpan = "<span class=\"glyphicon glyphicon-user\"></span> About Me";
        $menu->addChild($aboutSpan, array(
            'route'     => 'freytech_core.about',
            'extras'    => array('safe_label' => true),
        ));
        $menu[$aboutSpan]->setLinkAttribute('title', 'About Me');

        $contactSpan = "<span class=\"glyphicon glyphicon-envelope\"></span> Contact";
        $menu->addChild($contactSpan, array(
                'route'     => 'freytech_core.contact',
                'extras'    => array('safe_label' => true),
            ));
        $menu[$contactSpan]->setLinkAttribute('title', 'Contact Me');

        $resumeSpan = "<span class=\"glyphicon glyphicon-briefcase\"></span> Resume";
        $menu->addChild($resumeSpan, array(
                'route'     => 'freytech_core.resume',
                'extras'    => array('safe_label' => true),
            ));
        $menu[$resumeSpan]->setLinkAttribute('title', 'Resume');

        return $menu;
    }

    public function socialMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');

        $menu->setChildrenAttributes(array('class' => 'nav nav-stacked'));

        $twitterSpan = "<i class=\"fa fa-twitter fa-lg\"></i> Twitter";
        $menu->addChild($twitterSpan, array(
            'uri'        => 'http://www.twitter.com/brianfreytag',
            'extras'     => array('safe_label' => true),
            'attributes' => array('target' => '_blank')
        ));
        $menu[$twitterSpan]->setLinkAttribute('target', '_blank');

        $githubSpan = "<i class=\"fa fa-github fa-lg\"></i> GitHub";
        $menu->addChild($githubSpan, array(
            'uri'        => 'http://www.github.com/brianfreytag',
            'extras'     => array('safe_label' => true),
        ));
        $menu[$githubSpan]->setLinkAttribute('target', '_blank');

        $bitbucketSpan = "<i class=\"fa fa-bitbucket fa-lg\"></i> BitBucket";
        $menu->addChild($bitbucketSpan, array(
            'uri'        => 'http://code.freytag.technology',
            'extras'     => array('safe_label' => true),
        ));
        $menu[$bitbucketSpan]->setLinkAttribute('target', '_blank');

        $linkedInSpan = "<i class=\"fa fa-linkedin fa-lg\"></i> LinkedIn";
        $menu->addChild($linkedInSpan, array(
            'uri'        => 'https://www.linkedin.com/in/brianfreytag',
            'extras'     => array('safe_label' => true),
        ));
        $menu[$linkedInSpan]->setLinkAttribute('target', '_blank');

        return $menu;
    }
}
