<?php

namespace Freytech\Bundle\CommonBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Freytech\Bundle\CommonBundle\Entity\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Freytech\Bundle\CommonBundle\Entity\LoginHistory;

class AddFailedAttemptsListener
{
    /** @var \Freytech\Bundle\CommonBundle\Entity\Repository\UserRepository */
    protected $userRepository;
    /** @var \Doctrine\Common\Persistence\ObjectManager */
    protected $entityManager;
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /**
     * @param UserRepository $userRepository
     * @param ObjectManager $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(UserRepository $userRepository, ObjectManager $entityManager, ContainerInterface $container)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    public function onSecurityAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        if (!$event->getAuthenticationException() instanceof BadCredentialsException) {
            return;
        }
        /** @var \Freytech\Bundle\CommonBundle\Entity\User $user */
        $user = $event->getAuthenticationToken()->getUser();

        if (!$user instanceof UserInterface) {
            $user = $this->userRepository->findByUsername($user);
        }

        if ($user instanceof UserInterface) {
            $loginHistory = new LoginHistory();
            $loginHistory->setSuccess(false);
            $loginHistory->setUser($user);

            if ($this->container->isScopeActive('request')) {
                $request = $this->container->get('request');
                $loginHistory->setIpAddress($request->getClientIp());
            }

            $loginHistory->setPreviousFailedAttempts($user->getFailedAttempts());

            $user->addFailedAttempt();
            $user->setLastFailedDate(new \DateTime());

            $this->entityManager->persist($loginHistory);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
}