<?php

namespace Freytech\Bundle\CommonBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Doctrine\Common\Persistence\ObjectManager;
use Freytech\Bundle\CommonBundle\Entity\LoginHistory;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Handles the core functions after a successful login.
 *
 * Creates a new LoginHistory record, persists QLS data to the session,
 * calculates the user's credit track, and clears failed login data.
 *
 * @author Jeremy Livingston <jeremy@quizzle.com>
 */
class UpdateUserLoginListener
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $entityManager;

    /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface */
    protected $session;

    /**
     * @param ObjectManager $entityManager
     * @param SessionInterface $session
     */
    public function __construct(ObjectManager $entityManager, SessionInterface $session)
    {
        $this->entityManager        = $entityManager;
        $this->session              = $session;
    }

    /**
     * @param \Symfony\Component\Security\Http\Event\InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        //Set for Web Analytics
        $this->session->getFlashBag()->set('ft_just_logged_in', true);

        /* @var \Freytech\Bundle\CommonBundle\Entity\User $user */
        $user = $event->getAuthenticationToken()->getUser();
        $request = $event->getRequest();

        $loginHistory = new LoginHistory();
        $loginHistory->setSuccess(true);
        $loginHistory->setUser($user);
        $loginHistory->setIpAddress($request->getClientIp());
        $loginHistory->setPreviousFailedAttempts($user->getFailedAttempts());

        $this->entityManager->persist($loginHistory);

        $user->setFailedAttempts(0);
        $user->setLastFailedDate(null);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}