<?php

namespace Freytech\Bundle\CommonBundle\Mailer;

use Freytech\Bundle\CommonBundle\Entity\Core\ContactSubmission;
use Freytech\Bundle\CommonBundle\Entity\User;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

/**
 * Used to send e-mails to people that are awesome
 *
 * @author Brian Freytag <brian@idltg.in>
 */
class MailerService implements MailerInterface
{
    /** @var \Swift_Mailer */
    protected $mailer;
    /** @var \Twig_Environment */
    protected $twig;
    /** @var array */
    protected $parameters;

    /**
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     * @param array $parameters
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, array $parameters)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->parameters = $parameters;
    }

    public function sendContactFormEmail(ContactSubmission $contactSubmission)
    {
        $template = $this->parameters['contact']['template'];
        $contactName = $contactSubmission->getName();
        $contactEmail = $contactSubmission->getEmail();
        $contactSubject = $contactSubmission->getSubject();
        $contactMessage = $contactSubmission->getMessage();
        $sentDate = $contactSubmission->getCreateDate();

        $context = array(
            'contactName'    => $contactName,
            'contactEmail'   => $contactEmail,
            'contactSubject' => $contactSubject,
            'contactMessage' => $contactMessage,
            'sentDate'       => $sentDate
        );

        if (!$this->sendMessage($template, $context, array($this->parameters['from']['email'] => $this->parameters['from']['name']), $this->parameters['contact']['to_email']))
        {
            return false;
        }

        return true;
    }

    public function sendAccountVerificationEmail(User $user)
    {
        $template = $this->parameters['registration']['template'];
        $userName = $user->getFirstName();
        $userHash = $user->getAuthenticationHash();

        $context = array(
            'userName'  => $userName,
            'userHash'  => $userHash
        );

        if (!$this->sendMessage($template, $context, array($this->parameters['from']['email'] => $this->parameters['from']['name']), $user->getEmail()))
        {
            return false;
        }

        return true;
    }


    protected function sendMessage($templateName, $context, $fromEmail, $toEmail)
    {
        $context = $this->twig->mergeGlobals($context);
        /** @var \Twig_Template $template */
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html');
        } else {
            $message->setBody($textBody);
        }

        if (!$this->mailer->send($message)) {
            return false;
        }

        return true;
    }
}