<?php

namespace Freytech\Bundle\CommonBundle\Mailer;

use Freytech\Bundle\CommonBundle\Entity\Core\ContactSubmission;
use Freytech\Bundle\CommonBundle\Entity\User;

/**
 * Interface MailerInterface
 * @author Brian Freytag <brian@idltg.in>
 */
interface MailerInterface
{
    public function sendContactFormEmail(ContactSubmission $contactSubmission);

    public function sendAccountVerificationEmail(User $user);
}