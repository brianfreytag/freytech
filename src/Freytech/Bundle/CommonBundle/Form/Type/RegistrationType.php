<?php

namespace Freytech\Bundle\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Freytech\Bundle\CommonBundle\Entity\User;

/**
 * Class RegistrationType
 * @author Brian Freytag <brian@idltg.in>
 */
class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array('label'  => 'Choose a username'));

        $builder->add('email', 'repeated', array(
            'type'              => 'email',
            'invalid_message'   => 'The email addresses you have entered do not match, please enter the email address again.',
            'first_options'     => array('label' => 'Email Address'),
            'second_options'    => array('label' => 'Confirm Email')
        ));

        $builder->add('plainPassword', 'repeated', array(
            'type'              => 'password',
            'invalid_message'   => 'The new passwords you entered do not match each other.',
            'first_options'     => array('label' => 'Password'),
            'second_options'    => array('label' => 'Confirm Password'),
        ));

        $builder->add('firstName', 'text', array('label' => 'First Name'));
        $builder->add('lastName', 'text', array('label' => 'Last Name'));

        $builder->add('save', 'submit', array(
            'label' => 'Register',
            'attr' => array(
                'class' => 'btn btn-lg btn-primary'
            ),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Freytech\Bundle\CommonBundle\Entity\User',
            'validation_groups' => array('registration', 'Default'),
        ));
    }

    public function getName()
    {
        return 'ft_registration';
    }
}