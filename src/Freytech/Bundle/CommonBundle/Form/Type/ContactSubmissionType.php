<?php

namespace Freytech\Bundle\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ContactSubmissionType
 * @author Brian Freytag <brian@idltg.in>
 */
class ContactSubmissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text',
                array(
                    'label'  => 'What\'s your name?',
                ))
            ->add('email', 'email',
                array(
                    'label'  => 'What\'s your email?',
                 ))
            ->add('subject', 'text',
                array(
                    'label'  => 'What\'s this about?',
                ))
            ->add('message', 'textarea',
                array(
                    'label'  => 'What do you have to say about that?',
                ))
            ->add('save', 'submit')
            ->getForm();
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Freytech\Bundle\CommonBundle\Entity\Core\ContactSubmission',
            ));
    }

    public function getName()
    {
        return 'ft_contact_submission';
    }
}