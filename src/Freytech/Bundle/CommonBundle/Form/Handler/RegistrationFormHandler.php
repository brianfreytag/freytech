<?php

namespace Freytech\Bundle\CommonBundle\Form\Handler;

use Freytech\Bundle\CommonBundle\Entity\User;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class RegistrationFormHandler extends AbstractFormHandler
{
    /**
     * @param Request $request
     * @return bool
     */
    function process(Request $request)
    {
        $user = new User();

        $this->form = $this->formFactory->create($this->formTag, $user);

        if ($request->getMethod() === 'POST') {

            $this->form->submit($request);

            if ($this->form->isValid()) {
                return $this->onSuccess($request, $user);
            }

            $request->getSession()->getFlashBag()->add(
                'registration-failure',
                'There were errors in your registration. Please try again!'
            );
        }

        return false;
    }

    public function onSuccess(Request $request, User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        if (!$this->mailer->sendAccountVerificationEmail($user)) {
            $this->logger->critical('The registration form failed to send an e-mail');
        }

        $request->getSession()->getFlashBag()->add(
            'registration-email',
            'exists'
        );
        return true;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

}
