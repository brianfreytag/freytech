<?php

namespace Freytech\Bundle\CommonBundle\Form\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Freytech\Bundle\CommonBundle\Mailer\MailerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;

abstract class AbstractFormHandler
{
    /** @var FormFactoryInterface */
    protected $formFactory;
    /** @var \Symfony\Component\Form\FormInterface */
    protected $form;
    /** @var string */
    protected $formTag;
    /** @var ObjectManager */
    protected $entityManager;
    /** @var  MailerInterface $mailer */
    protected $mailer;
    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;


    /**
     * @param FormFactoryInterface $formFactory
     * @param ObjectManager $entityManager
     * @param MailerInterface $mailer
     * @param LoggerInterface $logger
     */
    public function __construct(FormFactoryInterface $formFactory, ObjectManager $entityManager, MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param $formTag
     */
    public function setFormTag($formTag)
    {
        $this->formTag = $formTag;
    }
}