<?php

namespace Freytech\Bundle\CommonBundle\Form\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Freytech\Bundle\CommonBundle\Entity\Core\ContactSubmission;
use Freytech\Bundle\CommonBundle\Mailer\MailerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class ContactSubmissionFormHandler extends AbstractFormHandler
{
    /**
     * @param Request $request
     * @return bool
     */
    function process(Request $request)
    {
        $contactSubmission = new ContactSubmission();
        $this->form = $this->formFactory->create($this->formTag, $contactSubmission);

        if ($request->getMethod() === 'POST') {

            $this->form->submit($request);

            if ($this->form->isValid()) {
                $this->entityManager->persist($this->form->getData());
                $this->entityManager->flush();
                if (!$this->mailer->sendContactFormEmail($this->form->getData())) {
                    $this->logger->critical('The contact form failed to send an e-mail');
                }
                $request->getSession()->getFlashBag()->add(
                    'contact-success',
                    'Thanks for contacting me. I will get back to you as soon as possible!'
                );

                return true;
            }

            $request->getSession()->getFlashBag()->add(
                'contact-failure',
                'There were errors in your submission. Please try again!'
            );
        }

        return false;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param $formTag
     */
    public function setFormTag($formTag)
    {
        $this->formTag = $formTag;
    }
}
