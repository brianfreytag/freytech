<?php

namespace Freytech\Bundle\AdminBundle\Form\Type;

use Freytech\Bundle\CommonBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form Type used to update a user
 *
 * @author Brian Freytag <brian@quizzle.com>
 */
class UpdateUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array('label'  => 'Change your username'));

        $builder->add('displayName', 'choice', array('choices' => $builder->getData()->getDisplayNames()));

        $builder->add('email', 'repeated', array(
            'type'              => 'email',
            'invalid_message'   => 'The email addresses you have entered do not match, please enter the email address again.',
            'first_options'     => array('label' => 'Email Address'),
            'second_options'    => array('label' => 'Confirm Email')
        ));

        $builder->add('plainPassword', 'repeated', array(
            'type'              => 'password',
            'invalid_message'   => 'The new passwords you entered do not match each other.',
            'first_options'     => array('label' => 'Password'),
            'second_options'    => array('label' => 'Confirm Password'),
        ));

        $builder->add('firstName', 'text', array('label' => 'First Name'));
        $builder->add('lastName', 'text', array('label' => 'Last Name'));

        $builder->add('save', 'submit', array(
            'label' => 'Update User',
            'attr' => array(
                'class' => 'btn btn-lg btn-primary'
            ),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Freytech\Bundle\CommonBundle\Entity\User',
            'validation_groups' => array('update_user', 'Default'),
        ));
    }

    public function getName()
    {
        return 'ft_update_user';
    }
}
