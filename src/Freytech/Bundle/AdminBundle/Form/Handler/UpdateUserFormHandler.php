<?php

namespace Freytech\Bundle\AdminBundle\Form\Handler;

use Freytech\Bundle\CommonBundle\Form\Handler\AbstractFormHandler;
use Freytech\Bundle\CommonBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UpdateUserFormHandler extends AbstractFormHandler
{
    /** @var EncoderFactoryInterface */
    private $encoderFactory;

    /**
     * @param Request $request
     * @param User $user
     *
     * @return bool
     */
    function process(Request $request, User $user)
    {
        $this->form = $this->formFactory->create($this->formTag, $user);

        if ($request->getMethod() === 'POST') {

            $this->form->submit($request);

            $newPassword = $this->form->getData()->getPlainPassword();

            if ($this->form->isValid()) {
                if (!empty($newPassword)) {
                    $encoder = $this->encoderFactory->getEncoder($user);
                    $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
                }
                $user->setFailedAttempts(0);
                $user->setLastFailedDate(null);

                return $this->onSuccess($request, $user);
            }

            $request->getSession()->getFlashBag()->add(
                'update-user-failure',
                'There were errors updating the user. Please try again!'
            );
        }

        return false;
    }

    public function onSuccess(Request $request, User $user)
    {

        $newPassword = $request->request->get('ft_update_user[plainPassword][first]');

        if (!empty($newPassword)) {
            $user->setPlainPassword($newPassword);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $request->getSession()->getFlashBag()->add(
            'update-user-success',
            'The user has been successfully updated!'
        );

        return true;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function setEncoderFactory(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }
}
