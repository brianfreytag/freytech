<?php

namespace Freytech\Bundle\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * @author Jeremy Livingston <jeremy@quizzle.com>
 */
class AdminMenuBuilder
{
    /** @var \Knp\Menu\FactoryInterface */
    private $factory;
    /** @var  SecurityContextInterface */
    private $context;

    /**
     * @param FactoryInterface $factory
     * @param SecurityContextInterface $context
     */
    public function __construct(FactoryInterface $factory, SecurityContextInterface $context)
    {
        $this->factory = $factory;
        $this->context = $context;
    }

    public function adminMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setCurrentUri($request->getRequestUri());

        $menu->setChildrenAttributes(array('class' => 'nav nav-stacked'));

        $dashboardSpan = "<span class=\"glyphicon glyphicon-cog\"></span> Dashboard";
        $menu->addChild($dashboardSpan, array(
            'route'     => 'freytech_admin.home',
            'extras'    => array('safe_label' => true),
        ));

        $accountSpan = "<span class=\"glyphicon glyphicon-user\"></span> Account";
        $menu->addChild($accountSpan, array(
            'route'     => 'freytech_admin.account',
            'extras'    => array('safe_label' => true),
        ));

        $menu->addChild('<hr>', array(
            'route'     => '',
            'class'     => 'divider',
            'extras'    => array('safe_label' => true),
        ));

        $logoutSpan = "<span class=\"glyphicon glyphicon-log-out\"></span> Log Out</a>";
        $menu->addChild($logoutSpan, array(
            'route'     => 'freytech_common.logout',
            'extras'    => array('safe_label' => true),
        ));
        return $menu;
    }

}